import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-formulario-empleados',
  templateUrl: './formulario-empleados.component.html',
  styleUrls: ['./formulario-empleados.component.css'],
  providers: [DatePipe]
})
export class FormularioEmpleadosComponent implements OnInit {
  listEmpleados: any[] = [];
  form : FormGroup;
  id : number | undefined;
  DateToday : Date;
  DateMonthAgo : Date;

  constructor(private fb: FormBuilder, 
    private toastr: ToastrService,
    private _empleadoService: EmpleadoService, 
    private datePipe: DatePipe
    ) {
    this.DateToday = new Date();
    this.DateMonthAgo = new Date();
    this.DateMonthAgo.setMonth(this.DateMonthAgo.getMonth() - 1);

    this.form = this.fb.group({
      primerApellido: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Z\s]+$')]],
      segundoApellido: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Z\s]+$')]],
      primerNombre: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Z\s]+$')]],
      otrosNombres: ['', [Validators.maxLength(50), Validators.pattern('^[A-Z/ \s]+$')]],
      tipoIdentificacion: ['', [Validators.required]],
      numeroIdentificacion: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*$')]],
      pais: ['', [Validators.required]],
      area: ['', [Validators.required]],
      fechaIngreso: ['', [Validators.required, this.dateRangeValidator]],
      fechaRegistro: [this.datePipe.transform(this.DateToday, 'dd-MM-yyyy HH:mm:ss'), [Validators.required]],
      estado: [{value: true, disabled: true}, [Validators.required]],
      correo: ['']
    })

  }

  ngOnInit(): void {
    this.listarEmpleados();
  }

  private dateRangeValidator: ValidatorFn = (): {
    [key: string]: any;
  } | null => {
    let invalid = false;
    const from = this.DateMonthAgo;
    const to = this.DateToday;
    
    if(this.form && this.form.get("fechaIngreso")?.value){
      if (from > new Date(this.form.get('fechaIngreso')?.value)) {
        invalid = true;
      }
      if (to < new Date(this.form.get('fechaIngreso')?.value)) {
        invalid = true;
      }
    }
    return invalid ? { invalidRange: { from, to } } : null;
  };

  listarEmpleados(){
    //Obtener listado de empleados
    try {
      this._empleadoService.getListEmpleados().subscribe({
        next: (data) => {console.log(data); 
                      this.listEmpleados = data},
        error: (e) => {console.error(e);
          this.toastr.error('Ocurrió un error: ' + e.error, 'Error');},
        complete: () => console.info('completado') 
      })
    } catch (error) {
      this.toastr.error('Ocurrió un error: ' + error, 'Error');
    }
    
  }

  registrarEmpleado(){
    console.log(this.form);
    const inputPais = document.querySelector("#input_pais") as HTMLSelectElement;
    let dominioCorreo: string = "co";
    let correo: string = "";

    //definir dominio de correo
    if(inputPais != null){
      dominioCorreo = inputPais[inputPais.selectedIndex].getAttribute("codigo") ?? 'co';
    }

    //generar dominio de correo del empleado, se genera el nombre del correo desde el backend
    if(this.id != undefined){
      correo = this.form.get('correo')?.value   
    }else{
      correo = `@cidenet.com.${dominioCorreo}`;
    }
    
    //obtener datos del formulario para el empleado a registrar
    const empleado: any = {
      primerApellido: this.form.get('primerApellido')?.value,
      segundoApellido: this.form.get('segundoApellido')?.value,
      primerNombre: this.form.get('primerNombre')?.value,
      otrosNombres: this.form.get('otrosNombres')?.value,
      tipoIdentificacion: this.form.get('tipoIdentificacion')?.value,
      numeroIdentificacion: this.form.get('numeroIdentificacion')?.value,
      pais: this.form.get('pais')?.value,
      area: this.form.get('area')?.value,
      fechaIngreso: this.form.get('fechaIngreso')?.value,
      fechaRegistro: this.form.get('fechaRegistro')?.value, 
      correo: correo,
      estado: this.form.get('estado')?.value  
    }

    //enviar la informacion del empleado para registrar
    if(this.form.valid){
      console.log(empleado);
      //Agregar nuevo empleado
      if(this.id == undefined){

        try {
          this._empleadoService.postEmpleado(empleado).subscribe({
            next: (data) => {console.log(data); 
              this.toastr.success('El empleado ha sido registrado!', 'Agregado');
              this.form.reset();
              this.form.patchValue({
                fechaRegistro: this.datePipe.transform(this.DateToday, 'dd-MM-yyyy HH:mm:ss')
              })
              this.listarEmpleados();},
            error: (e) => {console.error(e);
              this.toastr.error('Ocurrió un error: ' + e.error, 'Error');},
            complete: () => console.info('completado') 
          })
        } catch (error) {
          this.toastr.error('Ocurrió un error: ' + error, 'Error')
        }

      }else{
        //Actualizar empleado
        try {
          empleado.id = this.id;

          this._empleadoService.putEmpleado(this.id,empleado).subscribe({
            next: (data) => {console.log(data);
              this.toastr.info('Se ha actualizado la información del empleado', 'Actualizado');
              this.id = undefined;
              this.form.reset();
              this.form.patchValue({
                fechaRegistro: this.datePipe.transform(this.DateToday, 'dd-MM-yyyy HH:mm:ss')
              })
              this.listarEmpleados();},
            error: (e) => {console.error(e);
              this.toastr.error('Ocurrió un error: ' + e.error, 'Error');},
            complete: () => console.info('completado') 
          })
        } catch (error) {
          this.toastr.error('Ocurrió un error: ' + error, 'Error');
        }

      }

    }else{
      this.toastr.error('Por favor valide que la información sea correcta!', 'Información incorrecta');
    }
    
  }

  eliminarEmpleado(id: number){

    try {
      this._empleadoService.deleteEmpleado(id).subscribe({
        next: (data) => {console.log(data);
          this.toastr.success('El empleado ha sido eliminado!', 'Eliminado');
          this.listarEmpleados();},
        error: (e) => {console.error(e);
          this.toastr.error('Ocurrió un error: ' + e.error, 'Error');},
        complete: () => console.info('completado') 
      })
    } catch (error) {
      this.toastr.error('Ocurrió un error: ' + error, 'Error');
    }
    
  }

  editarEmpleado(empleado: any){
    this.id = empleado.id;

    this.form.patchValue({
      primerApellido: empleado.primerApellido,
      segundoApellido: empleado.segundoApellido,
      primerNombre: empleado.primerNombre,
      otrosNombres: empleado.otrosNombres,
      tipoIdentificacion: empleado.tipoIdentificacion,
      numeroIdentificacion: empleado.numeroIdentificacion,
      pais: empleado.pais,
      area: empleado.area,
      fechaIngreso: empleado.fechaIngreso,
      fechaRegistro: empleado.fechaRegistro,
      estado: empleado.estado,
      correo: empleado.correo
    })
    
    
    const TabPaneConsultar = document.querySelector('#pills-consultar');
    TabPaneConsultar?.setAttribute("class","tab-pane fade");
    const TabPaneEditar = document.querySelector('#pills-registrar');
    TabPaneEditar?.setAttribute("class","tab-pane fade show active");

    const ButtonTabConsultar = document.querySelector('button[data-bs-target="#pills-consultar"]');
    ButtonTabConsultar?.setAttribute("class","nav-link");
    const ButtonTabRegistrar = document.querySelector(' button[data-bs-target="#pills-registrar"]');
    ButtonTabRegistrar?.setAttribute("class","nav-link active");
  }

  limpiarFormulario(){
    this.form.reset();
    this.form.patchValue({
      fechaRegistro: this.datePipe.transform(this.DateToday, 'dd-MM-yyyy HH:mm:ss')
    })
  }
 
}
