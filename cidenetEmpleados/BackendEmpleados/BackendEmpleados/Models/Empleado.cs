﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BackendEmpleados.Models
{
    public class Empleado
    {
        public int Id { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "{0} debe ser de al menos {2} y máximo {1} caracteres", MinimumLength = 2)]
        [Display(Name = "Primer apellido")]
        public string primerApellido { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "{0} debe ser de al menos {2} y máximo {1} caracteres", MinimumLength = 2)]
        [Display(Name = "Segundo apellido")]
        public string segundoApellido { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "{0} debe ser de al menos {2} y máximo {1} caracteres", MinimumLength = 2)]
        [Display(Name = "Primer nombre")]
        public string primerNombre { get; set; }
        [StringLength(50, ErrorMessage = "{0} debe ser de al menos {2} y máximo {1} caracteres", MinimumLength = 2)]
        [Display(Name = "Otros nombres")]
        public string otrosNombres { get; set; }
        [Required]
        [Display(Name = "Tipo identificacion")]
        public string tipoIdentificacion { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "{0} debe ser al menos {2} y máximo {1} caracteres", MinimumLength = 6)]
        [Display(Name = "Numero identificacion")]
        public string numeroIdentificacion { get; set; }
        public string pais { get; set; }
        [Required]
        public string area { get; set; }
        [Required]
        [Display(Name = "Fecha ingreso")]
        public string fechaIngreso { get; set; }
        [Required]
        [Display(Name = "Fecha registro")]
        public string fechaRegistro { get; set; }
        [Required]
        public bool estado { get; set; }
        [Required]
        [StringLength(300, ErrorMessage = "{0} debe ser al menos {2} y máximo {1} caracteres", MinimumLength = 5)]
        public string correo { get; set; }
    }
}
