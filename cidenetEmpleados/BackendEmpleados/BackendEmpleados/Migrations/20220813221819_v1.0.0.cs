﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendEmpleados.Migrations
{
    public partial class v100 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Empleado",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    primerApellido = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    segundoApellido = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    primerNombre = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    otrosNombres = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    tipoIdentificacion = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    numeroIdentificacion = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    pais = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    area = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    fechaIngreso = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    fechaRegistro = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    estado = table.Column<bool>(type: "bit", nullable: false),
                    correo = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Empleado", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Empleado");
        }
    }
}
