﻿using BackendEmpleados.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BackendEmpleados.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadoController : ControllerBase
    {
        private readonly AplicationDbContext _context;

        public EmpleadoController(AplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/<EmpleadoController>
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            try
            {
                var listEmpleados = await _context.Empleado.ToListAsync();
                return Ok(listEmpleados);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET api/<EmpleadoController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<EmpleadoController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Empleado empleado)
        {
            try
            {
                //Validar si ya existe empleado con mismo número y tipo de identificacion
                int existeEmpleado = await _context.Empleado.Where(e => e.tipoIdentificacion == empleado.tipoIdentificacion && e.numeroIdentificacion == empleado.numeroIdentificacion).CountAsync();

                //En caso de ya existir se le concatena un id
                if (existeEmpleado > 0)
                {
                    return BadRequest("Ya existe un empleado registrado con el mismo tipo y número de identificación");
                }

                //Generar correo del empleado
                string sCorreo = await generarCorreo(empleado);
                empleado.correo = sCorreo;

                //Guardar informacion del empleado
                _context.Empleado.Add(empleado);
                await _context.SaveChangesAsync();

                return Ok(empleado);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<EmpleadoController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Empleado empleado)
        {
            try
            {
                if (empleado == null)
                {
                    return NotFound();
                }

                if (id != empleado.Id)
                {
                    return NotFound();
                }

                //Validar si ya existe empleado con mismo número y tipo de identificacion
                int existeEmpleado = await _context.Empleado.Where(e => e.tipoIdentificacion == empleado.tipoIdentificacion && e.numeroIdentificacion == empleado.numeroIdentificacion && e.Id != id).CountAsync();

                //En caso de ya existir se le concatena un id
                if (existeEmpleado > 0)
                {
                    return BadRequest("Ya existe un empleado registrado con el mismo tipo y número de identificación");
                }

                //Generar correo del empleado
                string sCorreo = await generarCorreo(empleado);
                empleado.correo = sCorreo;

                _context.Empleado.Update(empleado);
                await _context.SaveChangesAsync();

                return Ok(new { message = "El empleado a sido actualizado" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<EmpleadoController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                //Buscar informacion de empleado
                var empleado = await _context.Empleado.FindAsync(id);

                if (empleado == null)
                {
                    return NotFound();
                }

                //Eliminar informacion de empleado
                _context.Empleado.Remove(empleado);
                await _context.SaveChangesAsync();

                return Ok(new { message = "El empleado a sido eliminado" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private async Task<string> generarCorreo(Empleado empleado)
        {

            string sDominio = empleado.correo;
            string sCorreo = "";

            //si se está registrando o actualizando la informacion del empleado
            if (empleado.Id == 0)
            {
                sCorreo = string.Format("{0}.{1}{2}", empleado.primerNombre.Replace("/ / g", ""), empleado.primerApellido.Replace("/ / g", ""), sDominio);
            }
            else {
                var empleadoNoatualizado = await _context.Empleado.AsNoTracking().Where(x => x.Id == empleado.Id).FirstOrDefaultAsync();
                //validar si al actualizar se cambia el nombre o apellido de la persona
                if (empleadoNoatualizado.primerNombre != empleado.primerNombre || empleadoNoatualizado.primerApellido != empleado.primerApellido)
                {
                    sDominio = sDominio.Split('@')[1];
                    sCorreo = string.Format("{0}.{1}@{2}", empleado.primerNombre.Replace("/ / g", ""), empleado.primerApellido.Replace("/ / g", ""), sDominio);
                }
                else {
                    sCorreo = sDominio;
                }
            }

            //consultar si ya existe un correo igual registrado
            int? IdEmpleado = await _context.Empleado.Where(e => e.correo == sCorreo.ToLower()).MaxAsync(u => (int?)u.Id);

            //En caso de ya existir se le concatena un id
            if (IdEmpleado != null && IdEmpleado > 0)
            {
                //Si el correo ya existente pertenece al mismo empleado en caso de que se esté actualizando
                if (IdEmpleado == empleado.Id)
                {
                    sCorreo = empleado.correo;
                }
                else {
                    sCorreo = string.Format("{0}.{1}.{2}{3}", empleado.primerNombre, empleado.primerApellido, (IdEmpleado + 1), sDominio);
                }
            }

            return sCorreo.ToLower();
        }
    }
}
